package com.example.uaf.grocerystore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by UAF on 11/4/2017.
 */

class recycleadapter extends RecyclerView.Adapter<recycleadapter.recycler> {

    private Context mContext;
    private List<products> mproducts;
//    private String[] pname;
//    private int[] pprice;
//    private int[] quantity;
//    private int[] add_to_cart;
//    private int[] thumbnail;

    recycleadapter(Context mContext,List<products> mproducts)
    {
        this.mContext=mContext;
        this.mproducts=mproducts;
//        this.pname=pname;
//        this.pprice=pprice;
//        this.quantity=quantity;
//        this.add_to_cart=add_to_cart;
    }
    @Override
    public recycler onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_list,parent,false);
        return new recycler(view);
   }

    @Override
    public void onBindViewHolder(recycler holder, int position) {
        products oproducts = mproducts.get(position);

//        String name = pname[position];
//        int price =pprice[position];
//        int quantity =pprice[position];
        String a = String.valueOf(oproducts.getQuantity());
        String b = String.valueOf(oproducts.getPrice());

        holder.name.setText(oproducts.getName());
        holder.quantity.setText("Available: "+a);
        holder.price.setText("Rs: "+b);

        Glide.with(mContext).load(oproducts.getThumbnail()).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return mproducts.size();
    }

     class recycler extends RecyclerView.ViewHolder{

        ImageView thumbnail;
        ImageView add_cart;
        TextView name;
        TextView price;
        TextView quantity;

        recycler(View itemView) {

            super(itemView);

            thumbnail=(ImageView) itemView.findViewById(R.id.thumbnail);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView)itemView.findViewById(R.id.price);
            quantity=(TextView)itemView.findViewById(R.id.quantity);

            add_cart =(ImageView) itemView.findViewById(R.id.add_cart);



        }
    }
}
