package com.example.uaf.grocerystore;

/**
 * Created by UAF on 11/4/2017.
 */

public class products {

    private String name;
    private int price;
    private String quantity;
    private int thumbnail;
    private int add_cart;


    public products(String name,int price,String  quantity,int thumbnail)
    {
        this.name=name;
        this.price=price;
        this.quantity=quantity;
        this.thumbnail=thumbnail;
//        this.add_cart=add_cart;
    }

    public int getAdd_cart() {
        return add_cart;
    }

    public void setAdd_cart(int add_cart) {
        this.add_cart = add_cart;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }



}

